# Exercise03_step3

Step 3 of the Exercise03. This version displays a (hardcoded, fixed) list of Articles, and allowes the user to click on a title.

This version implements the same features of the previous step employing only 1 Activity, i.e. the MainActivity, while using Fragments
to build UI elements.

When a title is selected:

- it replaces the TitlesFragment with the DetailsFragmenty, which displays the title and description of the selected title, if the screen is in PORTRAIT (vertical) mode
- it displays both the titles and the selected article's details in the same screen, using two fragments (TitlesFragment and DetailsFragment).


