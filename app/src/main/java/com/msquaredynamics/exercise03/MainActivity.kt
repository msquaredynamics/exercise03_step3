package com.msquaredynamics.exercise03

import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE


data class Article(var id: Int, var title: String, var description: String)

val articles = listOf(
    Article(0, "Title01", "Description of title 01"),
    Article(1, "Title02","Description of title 02"),
    Article(2, "Title03","Description of title 03")
)

var selectedArticleId : Int = -1


class MainActivity : AppCompatActivity(), TitlesFragment.OnArticleSelectedListener {
    private var mDualPane = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mDualPane = resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE


        /* If we are in Portrait mode:
         * - We add the TitlesFragment if its not already there (this happens if the app started in landscape mode
         *   and is then switched into portrait mode. Once the Fragment is added once, the Activity's back stack will
         *   be recreated automatically on screen configuration changes)
         * - If an article is selected, we show its details (this will replace the fragment)
         *
         * If we are in landscape mode:
         * - If no article is selected, we remove any fragment from the details container on the right
         * - If an article is selected, we show its details
         */
        if (!mDualPane) {
            // Executed only when the app starts in Landscape and is then switched into Portrait
            if (supportFragmentManager.findFragmentByTag("titlesFragment") == null) {
                supportFragmentManager.beginTransaction()
                    .add(R.id.fragmentContainer_activity_main_portrait, TitlesFragment(), "titlesFragment")
                    .commit()
            }

            if (selectedArticleId != -1) {
                showDetails()
            }
        } else {
            if (selectedArticleId == -1) {
                supportFragmentManager.findFragmentById(R.id.detailscontainer_activity_main)?.apply {
                    supportFragmentManager.beginTransaction().remove(this).commit()
                }
            } else {
                showDetails()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putInt(KEY_SELECTED_ARTICLE, selectedArticleId)
        super.onSaveInstanceState(outState)
    }


    override fun onArticleSelected(id: Int) {
        selectedArticleId = id
        showDetails()
    }


    /**
     * Invoked to show details of a selected article.
     */
    private fun showDetails() {
        val id = selectedArticleId

        /*
        * When an article is selected, we need to check the screen orientation first.
        * If we are in portrait mode, we replace the content of the layout with a new DetailsFragment instance
        *
        * If we are in landscape mode, we replace the fragment currently visibile (if needed) with a new fragment
        */
        if (!mDualPane) {
            val details = DetailsFragment.newInstance(id)

            /* Remove the previous DetailsFrament from the back stack, if present. This fixes a potential bug caused by
             * overlapping of fragments. To reproduce it:
             * 1) Comment the line below (popBackStackImmediate)
             * 2) Start the app and in Portrait mode, and select one Title (Title01)
             * 3) Rotate the screen in landscape and select another Title (Title02)
             * 4) Rotate the screen back in Portrait. Now the system will restore the Activity and its view hierarchy.
             * Since now the screen is in Portrait, the onCreate() method will call showDetails, and since Title02
             * was selected, it will show Title02 as expected.
             * However, Title01 remained in the backstack of the activity's FragmentManager, so tapping the "back" button
             * will not return to the TitlesFragment view, but to DetailsFragment view, displaying Title02.
             * The popBackStackImmediate removes the previous transaction stored in the backstack called "detailsFragment",
             * avoiding this problem
             */
            supportFragmentManager.popBackStackImmediate("detailsFragment", POP_BACK_STACK_INCLUSIVE)

            supportFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainer_activity_main_portrait, details)
                addToBackStack("detailsFragment")
                commit()
            }
        } else {
            // Landscape orientation
            var details = supportFragmentManager.findFragmentById(R.id.detailscontainer_activity_main) as? DetailsFragment

            if (details?.shownId() != id) {
                // Current fragment displays another article. We need to replace it
                details = DetailsFragment.newInstance(id)
                supportFragmentManager.beginTransaction().apply {
                    replace(R.id.detailscontainer_activity_main, details)
                    commit()
                }
            }
        }
    }



    override fun onBackPressed() {
        if (mDualPane) {
            /*
             * Forces the activity to terminate. Otherwise, in landscape mode, the default system behaviour would
             * try to pop the last transaction from the backstack, and since transactions are saved in Portrait mode only,
             * it would result in a "no-op" for the user, since the operation would produce no UI changes.
             *
             */
            finish()
            return
        }

        selectedArticleId = -1
        super.onBackPressed()
    }


    companion object {
        @JvmStatic
        private val KEY_SELECTED_ARTICLE = "selectedArticleId"
    }
}
